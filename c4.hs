potencia :: Float -> Integer -> Float
potencia a 0 = 1
potencia a n = a * (potencia a (n-1))

sumImpSq4 :: Integer -> Integer
sumImpSq4 umbral = sumaAuxiliar umbral 1

sumaAuxiliar :: Integer -> Integer -> Integer
sumaAuxiliar umbral impar | impar ^ 2 > umbral = 0
			  | otherwise = impar + sumaAuxiliar umbral (impar+2)

division :: Integer -> Integer -> (Integer, Integer)
division a d | a < d = (0, a)
	     | otherwise = (fst qr + 1, snd qr)
	       where qr = division (a-d) d

suma :: [Integer] -> Integer
suma lista | length lista == 0 = 0
	   | otherwise = head lista + suma (tail lista)

productoria :: [Integer] -> Integer
productoria lista | length lista == 0 = 1
		  | otherwise = head lista * productoria (tail lista)

reverso :: [a] -> [a]
reverso lista | length lista == 0 = []
	      | otherwise = reverso (tail lista) ++ [head lista]

--[1,2,3,4]
--[4,3,2,1]

capicua :: [Integer] -> Bool
capicua lista = lista == reverso lista
