f x y z | y < 10 = x
	| otherwise = x + z

nand True True = False
nand True False = True
nand False True = True
nand False False = True

nand2 x y | (x == y) = not(x)
		  | otherwise = True

nor True True = False
nor False True = False
nor True False = False
nor False False = True

nor2 x y | (x == y) = not(x)
		 |  otherwise = False

raiz a b c = (-b + sqrt (b**2 - (4*a*c))) / (2*a)

esPitagorica a b c = (a^2 + b^2) == c^2

