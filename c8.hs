data Arbol = Hoja Integer | Ramificacion Arbol Integer Arbol deriving (Eq, Show)
data Dir = Der | Izq
data Arbolg t = Hojag t | Ramif (Arbolg t) t (Arbolg t)


esHoja :: Arbol -> Bool
esHoja (Hoja x) = True
esHoja (Ramificacion a b c) = False

sumaNodos :: Arbol -> Integer
sumaNodos (Hoja x) = x
sumaNodos (Ramificacion a x b) = x + sumaNodos a + sumaNodos b

altura :: Arbol -> Integer
altura (Hoja x) = 1
altura (Ramificacion a x b) = 1 + max (altura a) (altura b)

pertenece :: Integer -> Arbol -> Bool
pertenece n (Hoja x) = x == n
pertenece n (Ramificacion a x b) = (x == n) || (pertenece n a) || (pertenece n b)

busqueda :: [Dir] -> Arbol -> Integer
busqueda [] (Hoja x) = x
busqueda [] (Ramificacion a x b) = x
busqueda (Izq:xs) (Ramificacion a x b) = busqueda xs a
busqueda (Der:xs) (Ramificacion a x b) = busqueda xs b

esHojag :: Arbolg a -> Bool
esHojag 
