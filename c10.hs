data RT t = Rose t [RT t] deriving Show

r1 = Rose 20 []

r2 = Rose 20 [Rose 21 [], Rose 33 []]

r3 = Rose 1 [Rose 2 [], Rose 3 [Rose 4 [], Rose 5 [], Rose 6 []]]

r4 = Rose 1 [Rose 2 [Rose 3 [Rose 4 []], Rose 5 [Rose 6 [], Rose 7 []]]]

r7 = Rose 20 [Rose 30 [Rose 40 [r4]]]

raiz :: RT t -> t
raiz (Rose n x) = n

hijos :: RT t -> [RT t]
hijos (Rose n x) = x

sumarTodo :: Num t => RT t -> t
sumarTodo (Rose n []) = n
--sumarTodo (Rose n [x]) = n + (sumarTodo x)
sumarTodo (Rose n (x:xs)) = (sumarTodo x) + (sumarTodo (Rose n xs))

hojas :: RT t -> [t]
hojas (Rose n []) = [n]
hojas (Rose n (x:xs)) = h : (hojas x) ++ hs
			where h:hs = hojas (Rose n xs)

espejar :: RT t -> RT t
espejar (Rose n []) = Rose n []
espejar (Rose n x) = Rose n (subEspejar x)

subEspejar :: [RT t] -> [RT t]
subEspejar [] = []
--subEspejar [x] = [x]
subEspejar (x:xs) = (subEspejar xs) ++ [espejar x]

altura :: RT t -> Integer
altura (Rose n []) = 1
altura (Rose n x) = 1 + (alturaMaxima x)

alturaMaxima :: [RT t] -> Integer
alturaMaxima [] = 0
alturaMaxima (x:xs) = max (altura x) (alturaMaxima xs)

maximo :: Ord t => RT t -> t
maximo (Rose n []) = n
maximo (Rose n x) = max n (subMaximo x)

subMaximo :: Ord t => [RT t] -> t
subMaximo [(Rose n [])] = n
subMaximo (x:xs) = max (maximo x) (subMaximo xs)
