import Data.Char

pendiente :: (Float, Float) -> (Float, Float) -> Float
pendiente p1 p2 = ( (snd p1 - snd p2) / (fst p1 - fst p2) )

iniciales :: String -> String -> String
iniciales nom ape = [head nom] ++ ['.'] ++ [head ape] ++ ['.']
