divParcial :: Integer -> Integer -> [Integer]
divParcial n 1 = [1]
divParcial n m | mod n m == 0 = divParcial n (m-1) ++ [m]
	       | otherwise = divParcial n (m-1)

divisores :: Integer -> [Integer]
divisores n = divParcial n n

esPrimo :: Integer -> Bool
esPrimo n = length (divisores n) == 2

suma :: [Integer] -> [Integer] -> [Integer]
suma [] [] = []
suma l1 l2 = (head l1) + (head l2) : suma (tail l1) (tail l2)

prodInterno :: [Float] -> [Float] -> Float
prodInterno [] [] = 0
prodInterno l1 l2 = ((head l1) * (head l2)) + prodInterno (tail l1) (tail l2)

noTieneDivisoresHasta :: Integer -> Integer -> Bool
noTieneDivisoresHasta 1 n = True --o False
noTieneDivisoresHasta m n | m == n = False
			  | otherwise = (mod n m /= 0) && (noTieneDivisoresHasta (m-1) n)

esPrimo2 :: Integer -> Bool
esPrimo2 n = noTieneDivisoresHasta (n-1) n
