import Data.Char

empaquetar :: [Char] -> [[Char]]
empaquetar l | length l == 0 = []
	     | length l == 1 = [l]
	     | (head l) == (head (tail l)) = unir ([[head l] ++ [head (tail l)]] ++ empaquetar (tail (tail l)))
	     | otherwise = unir ([[head l]] ++ [[head (tail l)]] ++ empaquetar (tail (tail l)))

unir :: [[Char]] -> [[Char]]
unir l | length l <= 1 = l
       | (head (head l)) == (head (head (tail l))) = unir ([(head l) ++ (head (tail l))] ++ tail (tail l))
       | otherwise = [head l] ++ unir (tail l)
