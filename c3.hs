fib :: Integer -> Integer
fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

par :: Integer -> Bool
--par 0 = True
par 1 = False
par 2 = True
par n = par (n-2)

sumaImpares :: Integer -> Integer
sumaImpares 1 = 1
sumaImpares n = (2*n)-1 + sumaImpares (n-1) --2*n-1 es el impar numero n

multiTres :: Integer -> Bool
--multiTres 0 = True
multiTres 1 = False
multiTres 2 = False
multiTres 3 = True
multiTres n = multiTres (n-3)
