import Data.Char

type Racional = (Integer, Integer)
data Dia = Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo deriving (Show, Eq)


mcd :: Integer -> Integer -> Integer
mcd a b | a == 0 || b == 0 = max a b
	| otherwise = mcd b (mod a b)
	
first :: (a, b, c) -> a
first (x, y, z) = x

longitud :: [a] -> Integer
longitud [] = 0
longitud (x:[]) = 1
longitud (x:y:[]) = 2
longitud (x:y:z:[]) = 3
longitud (_:_:_:xs) = 3 + longitud xs

iniciales :: [Char] -> [Char] -> [Char]
iniciales nombre apellido = (n:a:[])
		where (n:_) = nombre
		      (a:_) = apellido

producto :: Racional -> Racional -> Racional
producto (a, b) (c, d) = (a * c, b * d)

igual :: Racional -> Racional -> Bool
igual (a, b) (c, d) = (fromInteger a) / (fromInteger b) == (fromInteger c) / (fromInteger d)

mayor :: Racional -> Racional -> Bool
mayor (a, b) (c, d) = (fromInteger a) / (fromInteger b) > (fromInteger c) / (fromInteger d)

esFinde :: Dia -> Bool
esFinde Sabado = True
esFinde Domingo = True
esFinde x = False

esHabil :: Dia -> Bool
esHabil x = not (esFinde x)

soloAlgebra :: [Dia] -> [Dia] --Martes, Miercoles, Viernes
soloAlgebra [] = []
soloAlgebra (Martes:xs) = Martes : (soloAlgebra xs)
soloAlgebra (Miercoles:xs) = Miercoles : (soloAlgebra xs)
soloAlgebra (Viernes:xs) = Viernes : (soloAlgebra xs)
soloAlgebra (x:xs) = soloAlgebra xs
