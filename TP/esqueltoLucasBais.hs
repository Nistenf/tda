-- DATOS Y SHOW
type Pixel = (Integer, Integer, Integer)
type PixelDelta = (Integer, Integer, Integer)
type Frame = [[Pixel]]

data Video = Iniciar Frame | Agregar Frame Video deriving Eq
instance Show Video
   where show (Iniciar f) = mostrarFrame f
         show (Agregar f v) = (mostrarFrame f) ++ "\n" ++ (show v)

type FrameComprimido = [(Integer, Integer, PixelDelta)]
data VideoComprimido = IniciarComp Frame | AgregarNormal Frame VideoComprimido | AgregarComprimido FrameComprimido VideoComprimido
instance Show VideoComprimido
   where show (IniciarComp f) = "INICIAL \n" ++ mostrarFrame f
         show (AgregarNormal f v) = "NO COMPRIMIDO \n" ++ (mostrarFrame f) ++ "\n" ++ (show v)
         show (AgregarComprimido f v) = "COMPRIMIDO \n" ++ (mostrarFrameComprimido f) ++ "\n" ++ (show v)

mostrarFrame :: Frame -> String
mostrarFrame [] = ""
mostrarFrame (x:xs) = (show x) ++ "\n" ++ (mostrarFrame xs)

mostrarFrameComprimido :: FrameComprimido -> String
mostrarFrameComprimido [] = ""
mostrarFrameComprimido (x:xs) = "\t" ++ (show x) ++ "\n" ++ (mostrarFrameComprimido xs)

-- Ejercicio 1/5
ultimoFrame :: Video -> Frame
ultimoFrame (Iniciar frame) = frame
ultimoFrame (Agregar frame video) = frame
-- *Main> ultimoFrame video0 == f1
-- True
-- Testeado Lucas Ok.

-- Ejercicio 2/5
norma :: (Integer, Integer, Integer) -> Float
norma (r,g,b) = sqrt(fromIntegral(r^2+g^2+b^2))
-- *Main> norma (10, 20, 30)
-- 37.416573
-- Testeado Lucas Ok.

-- Ejercicio 3/5
pixelsDiferentesEnFrame :: Frame -> Frame -> Float -> FrameComprimido
pixelsDiferentesEnFrame frame1 frame2 umbral = pixelsDiferentesEnFrameAux frame1 frame2 umbral 0

-- Funci�n auxiliar pixelsDiferentesEnFrameAux - Toma frame y el frame a comparar por "pixelsDiferentesEnFrame", retorna el FrameComprimido.
-- columna es un valor fijo para construir la posicion columna, se inicializa en 0
-- En cada recurrencia va construyendo el frame comprimido agregando cada fila por separado.
pixelsDiferentesEnFrameAux :: Frame -> Frame -> Float -> Integer-> FrameComprimido
pixelsDiferentesEnFrameAux (filaFrame1:[]) (filaFrame2:[]) umbral columna = pixelsDiferentesEnFila filaFrame1 filaFrame2 umbral columna 0
pixelsDiferentesEnFrameAux (filaFrame1:resto1) (filaFrame2:resto2) umbral columna = pixelsDiferentesEnFila filaFrame1 filaFrame2 umbral columna 0 ++ pixelsDiferentesEnFrameAux resto1 resto2 umbral (columna+1)

-- Funci�n auxiliar pixelsDiferentesEnFila - Toma una fila de un frame y la compara con la fila del frame a comparar por "pixelsDiferentesEnFrame", retorna Fila de FrameComprimido construido
-- columna es un valor fijo para construir la posicion columna
-- posicionEnfila se inicializa siempre en 0 y va aumentando en 1 con la recursi�n.
pixelsDiferentesEnFila :: [Pixel] -> [Pixel]-> Float -> Integer -> Integer -> FrameComprimido
pixelsDiferentesEnFila (pixel1:xs) (pixel2:xs2) umbral columna posicionEnFila   | (length xs == 0) && obtenerPixel /= (0,0,0) = [(columna, posicionEnFila, obtenerPixel)]
                                                                                | (length xs == 0) = []
                                                                                | obtenerPixel == (0,0,0) = pixelsDiferentesEnFila xs xs2 umbral columna (posicionEnFila+1)
                                                                                | otherwise = (columna, posicionEnFila, obtenerPixel) : pixelsDiferentesEnFila xs xs2 umbral columna (posicionEnFila+1)
                                                                                where obtenerPixel = comparaPixel pixel1 pixel2 umbral

-- Funci�n auxiliar comparaPixel - Toma 2 pixeles y si la norma de la diferencia es mayor al umbral retorna la diferencia, caso contrario retorna (0,0,0) como pixel de diferencia.
comparaPixel :: Pixel -> Pixel -> Float -> Pixel
comparaPixel (r,g,b) (r2,g2,b2) umbral | (norma calculaPixelDelta) > umbral = calculaPixelDelta
                                       | otherwise = (0,0,0)  -- no diff, pixel delta = (0,0,0)
                                       where calculaPixelDelta = (r-r2,g-g2,b-b2) -- PixelDelta

-- *Main> pixelsDiferentesEnFrame v1f1 v2f2 1
-- [(0,0,(3,3,3)),(0,1,(3,3,3)),(1,0,(3,3,3)),(1,2,(-3,-3,-3)),(2,1,(-3,-3,-3)),(2,2,(-3,-3,-3))]
-- Testeado Lucas Ok.

-- Ejercicio 4/5
comprimir :: Video -> Float -> Integer -> VideoComprimido
comprimir (Iniciar frame) _ _ = IniciarComp frame
comprimir (Agregar frame2 restoVideo) umbral diffMaxPixels  | fromIntegral (length diff) > diffMaxPixels = AgregarNormal frame2 (comprimir restoVideo umbral diffMaxPixels)
                                                            | otherwise = AgregarComprimido diff (comprimir restoVideo umbral diffMaxPixels)
                                                            where diff = pixelsDiferentesEnFrame frame2 (ultimoFrame restoVideo)  umbral

-- Soluci�n menos elegante, trabaja en base a frame anterior.
-- comprimir (Agregar frame2 (Iniciar frame1)) umbral diffMaxPixels | (fromIntegral (length diff)) > diffMaxPixels = AgregarNormal frame2 (IniciarComp frame1)
--                                                                 | otherwise = AgregarComprimido (diff) (IniciarComp frame1)
--                                                                 where diff = pixelsDiferentesEnFrame frame2 frame1 umbral
--
-- comprimir (Agregar frame2 (Agregar frame1 restoVideo)) umbral diffMaxPixels  | fromIntegral (length diff) > diffMaxPixels = AgregarNormal frame2 (comprimir (Agregar frame1 restoVideo) umbral diffMaxPixels)
--                                                                              | otherwise = AgregarComprimido diff (comprimir (Agregar frame1 restoVideo) umbral diffMaxPixels)
--                                                                              where diff = pixelsDiferentesEnFrame frame2 frame1 umbral
-- Testeado Lucas Ok. OJO, Solo con v1 y v2 por defecto, ver de testear mas variables.

-- Ejercicio 5/5
descomprimir :: VideoComprimido -> Video
descomprimir (IniciarComp frame) =  (Iniciar frame)
descomprimir (AgregarComprimido framec (IniciarComp frame)) = Agregar (aplicarCambio frame framec) (descomprimir (IniciarComp frame))
descomprimir (AgregarNormal frame videoComp) = Agregar frame (descomprimir videoComp)
descomprimir (AgregarComprimido framec (AgregarNormal frame videoComp)) = Agregar (aplicarCambio frame framec) (descomprimir videoComp)
descomprimir (AgregarComprimido framec (AgregarComprimido framec2 videoComp)) = Agregar (aplicarCambio (ultimoFrame result) framec) result
                                                                              where result = descomprimir (AgregarComprimido framec2 videoComp)
descomprimir _ = error "error"

-- Funciones provistas por la c�tedra
sumarCambios :: FrameComprimido -> FrameComprimido -> FrameComprimido
sumarCambios fc1 fc2 = [(i, j, sumar deltas (busqueda i j fc2)) | (i, j, deltas) <- fc1] ++
                       [(i, j, deltas) | (i, j, deltas) <- fc2, busqueda i j fc1 == (0,0,0)]
-- *Main> sumarCambios [(1,1,(2,2,2)),(2,2,(0,0,-1))] [(1,1,(-3,-3,-3)), (1,2,(1,1,1))]
-- [(1,1,(-1,-1,-1)),(2,2,(0,0,-1)),(1,2,(1,1,1))]

aplicarCambio :: Frame -> FrameComprimido -> Frame
aplicarCambio f fc = [ [nuevoVal f i j fc| j <- [0..length (f !! i) - 1]] | i <- [0..length f - 1]]
  where nuevoVal f i j fc = sumar ((f !! i) !! j) (busqueda (fromIntegral i) (fromIntegral j) fc)
--  *Main> aplicarCambio [[(1,1,1),(2,2,2)],[(3,3,3),(4,4,4)]] [(0, 1, (1,2,3))]
--  [[(1,1,1),(3,4,5)],[(3,3,3),(4,4,4)]]

busqueda :: Integer -> Integer -> FrameComprimido -> PixelDelta
busqueda i j [] = (0, 0, 0)
busqueda i j ((x, y, c) : cs) | x == i && j == y = c
                            | otherwise = busqueda i j cs

sumar :: PixelDelta -> PixelDelta -> PixelDelta
sumar (x,y,z) (x2,y2,z2) =  (x+x2,y+y2,z+z2)

-- PRUEBAS
p3 :: Pixel
p3 = (3,3,3)

p0 :: Pixel
p0 = (0,0,0)

-- Video 0:
f0 = [[p0, p0, p0], [p3, p3, p3]]
f1 = [[p3, p3, p3], [p3, p3, p3]]
video0 = Agregar f1 (Agregar f0 (Iniciar f0))

-- Video 1:  En la versi�n comprimida, todos los frames son comprimidos (salvo el inicial)

v1f1 :: Frame
v1f1 = [[p3, p3, p0, p0, p0],
	  [p3, p3, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v1f2 :: Frame
v1f2 = [[p0, p0, p0, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v1f3 :: Frame
v1f3 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p3, p3, p0],
	  [p0, p0, p3, p3, p0],
	  [p0, p0, p0, p0, p0]]

v1f4 :: Frame
v1f4 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p3]]


v1 :: Video
v1 = Agregar v1f4 (Agregar v1f3 (Agregar v1f2 (Iniciar v1f1)))

v1Comp :: VideoComprimido
v1Comp = comprimir v1 1 6


-- Video 2:  En la versi�n comprimida, s�lo los frames 2 y 4 son comprimidos

v2f1 :: Frame
v2f1 = [[p3, p3, p0, p0, p0],
	  [p3, p3, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v2f2 :: Frame
v2f2 = [[p0, p0, p0, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v2f3 :: Frame
v2f3 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p3, p3, p3],
	  [p0, p0, p3, p3, p0],
	  [p0, p0, p0, p0, p0]]

v2f4 :: Frame
v2f4 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p3],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p3]]


v2 :: Video
v2 = Agregar v2f4 (Agregar v2f3 (Agregar v2f2 (Iniciar v2f1)))

v2Comp :: VideoComprimido
v2Comp = comprimir v2 1 6