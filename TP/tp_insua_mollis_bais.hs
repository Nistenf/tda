-- DATOS Y SHOW
type Pixel = (Integer, Integer, Integer)
type PixelDelta = (Integer, Integer, Integer)
type Frame = [[Pixel]]

data Video = Iniciar Frame | Agregar Frame Video deriving Eq
instance Show Video
   where show (Iniciar f) = mostrarFrame f
         show (Agregar f v) = (mostrarFrame f) ++ "\n" ++ (show v)

type FrameComprimido = [(Integer, Integer, PixelDelta)]
data VideoComprimido = IniciarComp Frame | AgregarNormal Frame VideoComprimido | AgregarComprimido FrameComprimido VideoComprimido
instance Show VideoComprimido
   where show (IniciarComp f) = "INICIAL \n" ++ mostrarFrame f
         show (AgregarNormal f v) = "NO COMPRIMIDO \n" ++ (mostrarFrame f) ++ "\n" ++ (show v)
         show (AgregarComprimido f v) = "COMPRIMIDO \n" ++ (mostrarFrameComprimido f) ++ "\n" ++ (show v)

mostrarFrame :: Frame -> String
mostrarFrame [] = ""
mostrarFrame (x:xs) = (show x) ++ "\n" ++ (mostrarFrame xs)

mostrarFrameComprimido :: FrameComprimido -> String
mostrarFrameComprimido [] = ""
mostrarFrameComprimido (x:xs) = "\t" ++ (show x) ++ "\n" ++ (mostrarFrameComprimido xs)

-- Ejercicio 1/5
ultimoFrame :: Video -> Frame
ultimoFrame (Iniciar frame) = frame
ultimoFrame (Agregar frame video) = frame
-- *Main> ultimoFrame video0 == f1
-- True
-- Testeado Lucas Ok.

-- Ejercicio 2/5
norma :: (Integer, Integer, Integer) -> Float
norma (r,g,b) = sqrt(fromIntegral(r^2+g^2+b^2))
-- *Main> norma (10, 20, 30)
-- 37.416573
-- Testeado Lucas Ok.

-- Ejercicio 3/5 - Solución 1
pixelsDiferentesEnFrame :: Frame -> Frame -> Float -> FrameComprimido
pixelsDiferentesEnFrame frame1 frame2 umbral = pixelsDiferentesEnFrameAux frame1 frame2 umbral 0

-- Función auxiliar pixelsDiferentesEnFrameAux - Toma frame y el frame a comparar por "pixelsDiferentesEnFrame", retorna el FrameComprimido.
-- columna es un valor fijo para construir la posicion columna, se inicializa en 0
-- En cada recurrencia va construyendo el frame comprimido agregando cada fila por separado.
pixelsDiferentesEnFrameAux :: Frame -> Frame -> Float -> Integer-> FrameComprimido
pixelsDiferentesEnFrameAux (filaFrame1:[]) (filaFrame2:[]) umbral columna = pixelsDiferentesEnFila filaFrame1 filaFrame2 umbral columna 0
pixelsDiferentesEnFrameAux (filaFrame1:resto1) (filaFrame2:resto2) umbral columna = pixelsDiferentesEnFila filaFrame1 filaFrame2 umbral columna 0 ++ pixelsDiferentesEnFrameAux resto1 resto2 umbral (columna+1)

-- Función auxiliar pixelsDiferentesEnFila - Toma una fila de un frame y la compara con la fila del frame a comparar por "pixelsDiferentesEnFrame", retorna Fila de FrameComprimido construido
-- columna es un valor fijo para construir la posicion columna
-- posicionEnfila se inicializa siempre en 0 y va aumentando en 1 con la recursión.
pixelsDiferentesEnFila :: [Pixel] -> [Pixel]-> Float -> Integer -> Integer -> FrameComprimido
pixelsDiferentesEnFila (pixel1:xs) (pixel2:xs2) umbral columna posicionEnFila   | (length xs == 0) && obtenerPixel /= (0,0,0) = [(columna, posicionEnFila, obtenerPixel)]
                                                                                | (length xs == 0) = []
                                                                                | obtenerPixel == (0,0,0) = pixelsDiferentesEnFila xs xs2 umbral columna (posicionEnFila+1)
                                                                                | otherwise = (columna, posicionEnFila, obtenerPixel) : pixelsDiferentesEnFila xs xs2 umbral columna (posicionEnFila+1)
                                                                                where obtenerPixel = comparaPixel pixel1 pixel2 umbral

-- Función auxiliar comparaPixel - Toma 2 pixeles y si la norma de la diferencia es mayor al umbral retorna la diferencia, caso contrario retorna (0,0,0) como pixel de diferencia.
comparaPixel :: Pixel -> Pixel -> Float -> Pixel
comparaPixel (r,g,b) (r2,g2,b2) umbral | (norma calculaPixelDelta) > umbral = calculaPixelDelta
                                       | otherwise = (0,0,0)  -- no diff, pixel delta = (0,0,0)
                                       where calculaPixelDelta = (r-r2,g-g2,b-b2) -- PixelDelta

-- END Solución 1

-- Solución 2

-- Ejercicio 3/5
--pixelsDiferentesEnFrame :: Frame -> Frame -> Float -> FrameComprimido
--pixelsDiferentesEnFrame [] [] u = []
--pixelsDiferentesEnFrame f1 f2 u = eliminaMenoresUmbral (diferenciasTotales 0 0 f1 f2) u

--eliminaMenoresUmbral: Toma un FrameComprimido y un umbral y elimina los cambios menores al umbral
--eliminaMenoresUmbral :: FrameComprimido -> Float -> FrameComprimido
--eliminaMenoresUmbral ((x, y, delta):[]) u | (norma delta) > u = [(x, y, delta)]
-- 										  | otherwise = []
--eliminaMenoresUmbral ((x, y, delta):fs) u | (norma delta) > u = [(x, y, delta)] ++ (eliminaMenoresUmbral fs u)
-- 										  | otherwise = (eliminaMenoresUmbral fs u)

--diferenciasTotales: toma dos Frames y devuelve un FrameComprimido con TODAS las diferencias entre pixeles. nf y nc son los indices de fila y columna, respectivamente
--diferenciasTotales :: Integer -> Integer -> Frame -> Frame -> FrameComprimido
--diferenciasTotales nf nc (frame1f1:[]) (frame2f1:[]) = (diferenciasFila nf nc frame1f1 frame2f1)
--diferenciasTotales nf nc (frame1f1:frame1s) (frame2f1:frame2s) = (diferenciasFila nf nc frame1f1 frame2f1) ++ (diferenciasTotales (nf+1) 0 frame1s frame2s)

--diferenciasFila: toma dos [Pixel] (filas de dos Frames) y devuelve todas las diferencias entre esas dos filas
--diferenciasFila :: Integer -> Integer -> [Pixel] -> [Pixel] -> FrameComprimido
--diferenciasFila nf nc ((r1, g1, b1):[]) ((r2, g2, b2):[]) = [(nf, nc, (r1-r2, g1-g2, b1-b2))]
--diferenciasFila nf nc ((r1, g1, b1):p1s) ((r2, g2, b2):p2s) = [(nf, nc, (r1-r2, g1-g2, b1-b2))] ++ (diferenciasFila nf (nc+1) p1s p2s)

-- END Solución 2

-- Ejercicio 4/5 
comprimir :: Video -> Float -> Integer -> VideoComprimido
comprimir (Iniciar frame) _ _ = IniciarComp frame
comprimir (Agregar frame2 restoVideo) umbral diffMaxPixels  | fromIntegral (length diff) > diffMaxPixels = AgregarNormal frame2 (comprimir restoVideo umbral diffMaxPixels)
                                                            | otherwise = AgregarComprimido diff (comprimir restoVideo umbral diffMaxPixels)
                                                            where diff = pixelsDiferentesEnFrame frame2 (ultimoFrame restoVideo)  umbral

-- END Ejercicio 4

-- Ejercicio 5/5
descomprimir :: VideoComprimido -> Video
descomprimir (IniciarComp f) = Iniciar f
descomprimir (AgregarComprimido f v) = Agregar (obtenerFrame f v) (descomprimir v)
descomprimir (AgregarNormal frame v) = Agregar frame (descomprimir v)

--Toma un FrameComprimido y un VideoComprimido (que va a ser el resto del video al que pertenece el FrameComp) y devuelve el FrameComprimido inicial como Frame sin comprimir
obtenerFrame :: FrameComprimido -> VideoComprimido -> Frame
obtenerFrame fc (IniciarComp f) = aplicarCambio f fc
obtenerFrame fc (AgregarNormal f v) = aplicarCambio f fc
obtenerFrame fc (AgregarComprimido fc2 v) = aplicarCambio (obtenerFrame fc2 v) fc

-- END Ejercicio 5

-- Funciones provistas por la cátedra
sumarCambios :: FrameComprimido -> FrameComprimido -> FrameComprimido
sumarCambios fc1 fc2 = [(i, j, sumar deltas (busqueda i j fc2)) | (i, j, deltas) <- fc1] ++
                       [(i, j, deltas) | (i, j, deltas) <- fc2, busqueda i j fc1 == (0,0,0)]

aplicarCambio :: Frame -> FrameComprimido -> Frame
aplicarCambio f fc = [ [nuevoVal f i j fc| j <- [0..length (f !! i) - 1]] | i <- [0..length f - 1]]
  where nuevoVal f i j fc = sumar ((f !! i) !! j) (busqueda (fromIntegral i) (fromIntegral j) fc)
--  *Main> aplicarCambio [[(1,1,1),(2,2,2)],[(3,3,3),(4,4,4)]] [(0, 1, (1,2,3))]
--  [[(1,1,1),(3,4,5)],[(3,3,3),(4,4,4)]]

busqueda :: Integer -> Integer -> FrameComprimido -> PixelDelta
busqueda i j [] = (0, 0, 0)
busqueda i j ((x, y, c) : cs) | x == i && j == y = c
                            | otherwise = busqueda i j cs

sumar :: PixelDelta -> PixelDelta -> PixelDelta
sumar (x,y,z) (x2,y2,z2) =  (x+x2,y+y2,z+z2)

-- PRUEBAS
p3 :: Pixel
p3 = (3,3,3)

p0 :: Pixel
p0 = (0,0,0)

-- Video 0:
f0 = [[p0, p0, p0], [p3, p3, p3]]
f1 = [[p3, p3, p3], [p3, p3, p3]]
video0 = Agregar f1 (Agregar f0 (Iniciar f0))

-- Video 1:  En la versión comprimida, todos los frames son comprimidos (salvo el inicial)

v1f1 :: Frame
v1f1 = [[p3, p3, p0, p0, p0],
	  [p3, p3, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v1f2 :: Frame
v1f2 = [[p0, p0, p0, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v1f3 :: Frame
v1f3 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p3, p3, p0],
	  [p0, p0, p3, p3, p0],
	  [p0, p0, p0, p0, p0]]

v1f4 :: Frame
v1f4 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p3]]


v1 :: Video
v1 = Agregar v1f4 (Agregar v1f3 (Agregar v1f2 (Iniciar v1f1)))

v1Comp :: VideoComprimido
v1Comp = comprimir v1 1 6


-- Video 2:  En la versión comprimida, sólo los frames 2 y 4 son comprimidos

v2f1 :: Frame
v2f1 = [[p3, p3, p0, p0, p0],
	  [p3, p3, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v2f2 :: Frame
v2f2 = [[p0, p0, p0, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p3, p3, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0]]

v2f3 :: Frame
v2f3 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p3, p3, p3],
	  [p0, p0, p3, p3, p0],
	  [p0, p0, p0, p0, p0]]

v2f4 :: Frame
v2f4 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p3],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p3]]


v2 :: Video
v2 = Agregar v2f4 (Agregar v2f3 (Agregar v2f2 (Iniciar v2f1)))

v2Comp :: VideoComprimido
v2Comp = comprimir v2 1 6

--Video 3

v3f1 :: Frame
v3f1 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p3],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p3]]

v3f2 :: Frame
v3f2 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p3],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p3]]

v3f3 :: Frame
v3f3 = [[p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p0],
	  [p0, p0, p0, p0, p3],
	  [p0, p0, p0, p3, p3],
	  [p0, p0, p0, p3, p0]]

v3 :: Video
v3 = Agregar v3f3 (Agregar v3f2 (Iniciar v3f1))