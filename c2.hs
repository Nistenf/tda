crearPar :: a -> b -> (a,b)
crearPar x y = (x, y)

invertir :: (a,b) -> (b, a)
invertir (x, y) = (y, x)

invertir2 :: (a,b) -> (b,a)
invertir2 x = (snd x, fst x)

distancia :: (Float, Float) -> (Float, Float) -> Float
distancia p1 p2 = sqrt ((fst p1 - fst p2)^2 + (snd p1 - snd p2)^2)

raices :: Float -> Float -> Float -> (Float, Float)
raices a b c = ( (-b + sqrt (b^2 - 4*a*c)) / (2*a), (-b - sqrt (b^2 - 4*a*c)) / (2*a) ) 

ite :: Bool -> a -> a -> a
ite c x y | (c == True) = x
	  | otherwise = y

listar :: a -> a -> a -> [a]
listar x y z = [x, y, z]

rangoDePaso :: Integer -> Integer -> Integer -> [Integer]
rangoDePaso x y z = [x, x+z .. y]
