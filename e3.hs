doblefact :: Integer -> Integer
doblefact 1 = undefined
doblefact 2 = 2
doblefact n = n * doblefact (n-2)

combi :: Integer -> Integer -> Integer
combi n m | m == 0 = 1
	  | n == 0 = 0
	  | otherwise = (combi (n-1) m) + (combi (n-1) (m-1))

mecuelgo :: Integer -> Integer
mecuelgo 0 = 1
mecuelgo n = mecuelgo (n-1)

sumarDeDos :: Integer -> Integer
sumarDeDos 0 = 0
sumarDeDos 1 = 1
sumarDeDos n = n + sumarDeDos (n-2)

sumImpSq :: Integer -> Integer
sumImpSq n | n == 0 = 0
	   | n == 1 = 1 
	   | mod (floor (sqrt (fromIntegral(n)))) 2 == 1 =  sumarDeDos (floor (sqrt (fromIntegral n))) 
	   | otherwise = sumarDeDos (floor (sqrt (fromIntegral n)) - 1)
	   

cuadAImpar :: Integer -> Integer
cuadAImpar n | mod (floor (sqrt (fromIntegral n))) 2 == 1 = floor (sqrt(fromIntegral n))
	     | otherwise = floor (sqrt(fromIntegral n)) - 1

sumImpSq2 :: Integer -> Integer --Igual a la 1
sumImpSq2 0 = 0
sumImpSq2 1 = 1
sumImpSq2 n = sumarDeDos (cuadAImpar n)
	   
sumImpSq3 :: Integer -> Integer
sumImpSq3 0 = 0
sumImpSq3 1 = 1
sumImpSq3 n = cuadAImpar n + sumImpSq3 (((cuadAImpar n) - 2) ^ 2)
