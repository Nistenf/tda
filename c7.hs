data Direccion = Norte | Sur | Este | Oeste
type Tortuga = (Pos, Direccion)
type Pos = (Integer, Integer)

arranca :: Tortuga
arranca = ((0,0), Norte)

girarDerecha :: Tortuga -> Tortuga
girarDerecha (p, Norte) = (p, Este)
girarDerecha (p, Este) = (p, Sur)
girarDerecha (p, Sur) = (p, Oeste)
girarDerecha (p, Oeste) = (p, Norte)

avanzar :: Tortuga -> Integer -> Tortuga
avanzar ((x, y), Norte) n = ((x, y+n), Norte)
avanzar ((x, y), Este) n = ((x+n, y), Este)
avanzar ((x, y), Sur) n = ((x, y-n), Sur)
avanzar ((x, y), Oeste) n = ((x-n, y), Oeste)

---------------------------------------------------------

data Figura = Rectangulo Float Float Float Float
	    | Circulo Float Float Float deriving (Show)

c1 :: Figura
c1 = Circulo 0 0 pi

r1 :: Float -> Figura
r1 x = Rectangulo 0 0 (cos(pi/4) * x) (sin (pi/4) * x)

area :: Figura -> Float
area (Rectangulo x1 y1 x2 y2) = abs(x2-x1) * abs(y2-y1)
area (Circulo x y r) = pi * (r^2)

perimetro :: Figura -> Float
perimetro (Circulo x y r) = 2 * r * pi
perimetro (Rectangulo x1 y1 x2 y2) = (abs(x2-x1) * 2) + (abs(y2-y1) * 2)

data Punto = Point Float Float deriving Show
data Figura2 = Rectangulo2 Punto Punto | Circulo2 Punto Float deriving Show

area2 :: Figura2 -> Float
area2 (Rectangulo2 (Point x1 y1) (Point x2 y2)) = abs(x2-x1) * abs(y2-y1)
area2 (Circulo2 p r) = pi * (r ^ 2)

perimetro2 :: Figura2 -> Float
perimetro2 (Circulo2 p r) = 2 * r * pi
perimetro2 (Rectangulo2 (Point x1 y1) (Point x2 y2)) = (abs(x2-x1) * 2) + (abs(y2-y1) * 2)

-----------------------------------------------------------------------------

data ProgAritmetica = Vacio | CongruentesA Integer Integer
instance Show ProgAritmetica where
	show Vacio = "{}"
--	show (CongruentesA x d) = "{a en Z | a = " ++ (show x) ++ " (mod " ++ (show d) ++ ")}"
	show (CongruentesA x d) = "{..., " ++ (show ((rem x d) - d)) ++ ", " ++ (show (rem x d)) ++ ", " ++ (show ((rem x d) + d)) ++ ", ...}"


esMultiplo :: Integer -> Integer -> Bool
esMultiplo x n = mod x n == 0

pertenece :: Integer -> ProgAritmetica -> Bool
pertenece x Vacio = False
pertenece x (CongruentesA n m) = (rem x m) == (rem n m)

incluido :: ProgAritmetica -> ProgAritmetica -> Bool
incluido Vacio _ = True --incluido Vacio Vacio entra en esta
incluido _ Vacio = False
incluido (CongruentesA n1 m1) (CongruentesA n2 m2) = ((rem n1 m2) == (rem n2 m2)) && (esMultiplo m1 m2)
