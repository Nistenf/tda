pertenece :: Integer -> [Integer] -> Bool
pertenece n [] = False
pertenece n lista | head lista == n = True
		  | otherwise = pertenece n (tail lista)

hayRepetidos :: [Integer] -> Bool
hayRepetidos [] = False
hayRepetidos lista | pertenece (head lista) cola = True
		   | otherwise = hayRepetidos cola
		     where cola = tail lista

menores :: Integer -> [Integer] -> [Integer]
menores n [] = []
menores n lista | (kbz < n) == True = kbz : menores n cola
		| otherwise = menores n cola
		  where kbz = head lista
			cola = tail lista

pertenece2 :: Integer -> [Integer] -> Bool --Usando or
pertenece2 n [] = False
pertenece2 n lista = (head lista == n) || (pertenece2 n (tail lista))

hayRepetidos2 :: [Integer] -> Bool --Usando or
hayRepetidos2 [] = False
hayRepetidos2 lista = (pertenece2 (head lista) cola) || hayRepetidos2 cola
		      where cola = tail lista

quitar :: Integer -> [Integer] -> [Integer] --Elimina la primera aparicion de n en lista
quitar n [] = []
quitar n lista | n == (head lista) = tail lista
	       | otherwise = (head lista) : (quitar n (tail lista))


maximo :: [Integer] -> Integer --mayor elemento de la lista
maximo lista | length lista == 1 = kbz
	     | otherwise = if (kbz > maximoCola) then kbz else maximoCola
	       where kbz = head lista
		     maximoCola = maximo (tail lista)

maximo2 :: [Integer] -> Integer 		--sin if
maximo2 lista | length lista == 1 = kbz
	      | kbz > maximoCola = kbz
	      | otherwise = maximoCola
	        where kbz = head lista
		      maximoCola = maximo2 (tail lista)

enBase :: Integer -> Integer -> [Integer] --pase n a base b. digitos en la lista
enBase n b | n < b = [n]
	   | otherwise = (enBase (div n b) b) ++ [mod n b]

deBase :: Integer -> [Integer] -> Integer --agarra b y lista de digitos y devuelve en b10
deBase b [] = 0
deBase b numero = (head numero) * (b ^ ((length numero) - 1)) + deBase b (tail numero)
