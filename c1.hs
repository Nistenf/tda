doble x = 2*x
suma x y = x + y
normaVectorial v1 v2 = sqrt (v1^2+v2^2)
funcionConstante8 _ = 8
respuestaAtodo = 42	

signo (n) | n > 0 = 1
		  | n == 0 = 0
		  | n < 0 = -1

absoluto x | x >= 0 = x
		   | otherwise = -x

absoluto2 x = (signo x) * x

max2 x y | x > y = x
	 	 | y >= x = y

max3 x y z | (max2 x y) > z = max2 x y
           | (max2 x y) <= z = z

max3o x y z = max2 (max2 x y) z

ylogico x y | x == False = False
			| y == False = False
			| otherwise = True

ylogico2 True True = True
ylogico2 False False = False
ylogico2 True False = False
ylogico2 False True = False

ylogico3 True True = True
ylogico3 x y = False

ylogico4 x y | (x == y) = x
			 | otherwise = False