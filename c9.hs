--Implementacion de ellos: https://goo.gl/3cXw0n

data Polinomio = Mono Float Integer
	       | Suma Polinomio Polinomio
	       | Producto Polinomio Polinomio
		deriving Show

data Polinomiog t = Monog t t
		  | Sumag (Polinomiog t) (Polinomiog t)
		  | Productog (Polinomiog t) (Polinomiog t)


evaluar :: Polinomio -> Float -> Float
evaluar (Mono a n) z = a * (z ^ n)
evaluar (Suma a b) z = (evaluar a z) + (evaluar b z)
evaluar (Producto a b) z = (evaluar a z) * (evaluar b z)

--evaluarg :: Num a => Polinomiog a -> a -> a
--evaluarg (Monog a n) z = a * (z ^ n)
--evaluarg (Sumag a b) z = (evaluarg a z) + (evaluarg b z)
--evaluarg (Productog a b) z = (evaluarg a z) * (evaluarg b z)

sumaCoes :: [Float] -> [Float] -> [Float]
sumaCoes [] y = y
sumaCoes x [] = x
sumaCoes (x:xs) (y:ys) = [x+y] ++ (sumaCoes xs ys)

aPoli :: [Float] -> Polinomio
aPoli [x] = Mono x 0
aPoli x = Suma (aPoli (init x)) (Mono (last x) ((fromIntegral(length x))-1))

multiPoli :: Polinomio -> Polinomio -> Polinomio
multiPoli (Mono a n) (Mono b m) = Mono (a*b) (n+m)
multiPoli p1 p2 = Suma (multiPoli (Mono (a0) 0) p2) (Producto (Mono 1 1) (Producto (aPoli (as)) p2))
		  where a0:as = coeficientes p1


coeficientes :: Polinomio -> [Float]
coeficientes (Mono a 0) = [a]
coeficientes (Mono a n) = (coeficientes (Mono 0 (n-1))) ++ [a]
coeficientes (Suma p1 p2) = sumaCoes (coeficientes p1) (coeficientes p2)
coeficientes (Producto p1 p2) = coeficientes (multiPoli p1 p2)

--a c9bis.hs
