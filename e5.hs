pertenece :: Integer -> [Integer] -> Bool
pertenece n [] = False
pertenece n lista = (head lista == n) || (pertenece n (tail lista))

enBase :: Integer -> Integer -> [Integer] --pase n a base b. digitos en la lista
enBase n b | n < b = [n]
           | otherwise = (enBase (div n b) b) ++ [mod n b]

deBase :: Integer -> [Integer] -> Integer --agarra b y lista de digitos y devuelve en b10
deBase b [] = 0
deBase b numero = (head numero) * (b ^ ((length numero) - 1)) + deBase b (tail numero)

capicuaPara :: [Integer] -> [Integer]
capicuaPara xs | xs == reverse xs = xs
	       | otherwise = capicuaPara (enBase ((deBase 10 xs) + (deBase 10 (reverse xs))) 10)

cambioBase :: Integer -> Integer -> [Integer] -> [Integer]
cambioBase b1 b2 xs | b1 == b2 = xs
		    | otherwise = enBase (deBase b1 xs) b2

ordNoDecr :: [Integer] -> Bool
ordNoDecr xs | length xs <= 1 = True
	     | otherwise = (head xs) <= (head (tail xs)) && ordNoDecr (tail xs)

repeUlti :: [Integer] -> [Integer]
repeUlti xs | length xs <= 1 = xs
	    | pertenece (head xs) (tail xs) = repeUlti (tail xs)
	    | otherwise = [head xs] ++ repeUlti (tail xs)

repeFst :: [Integer] -> [Integer]
repeFst xs | length xs <= 1 = xs
	   | 
